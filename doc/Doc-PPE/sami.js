
(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:GsbBundle" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle.html">GsbBundle</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:GsbBundle_Controller" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Controller.html">Controller</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_Controller_ComptaController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Controller/ComptaController.html">ComptaController</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Controller_GsbController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Controller/GsbController.html">GsbController</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Controller_SecurityController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Controller/SecurityController.html">SecurityController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:GsbBundle_DependencyInjection" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/DependencyInjection.html">DependencyInjection</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_DependencyInjection_GsbBundleExtension" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/DependencyInjection/GsbBundleExtension.html">GsbBundleExtension</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:GsbBundle_Entity" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Entity.html">Entity</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_Entity_Etat" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/Etat.html">Etat</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Entity_FicheFrais" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/FicheFrais.html">FicheFrais</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Entity_FraisForfait2" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/FraisForfait2.html">FraisForfait2</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Entity_LigneFraisForfait" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/LigneFraisForfait.html">LigneFraisForfait</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Entity_LigneFraisHorsForfait" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/LigneFraisHorsForfait.html">LigneFraisHorsForfait</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Entity_User" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Entity/User.html">User</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:GsbBundle_Form" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Form.html">Form</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:GsbBundle_Form_Compta" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Form/Compta.html">Compta</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_Form_Compta_AdministrerHorsForfaitType" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="GsbBundle/Form/Compta/AdministrerHorsForfaitType.html">AdministrerHorsForfaitType</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Form_Compta_ComptaConsulterType" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="GsbBundle/Form/Compta/ComptaConsulterType.html">ComptaConsulterType</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Form_Compta_FicheFraisType" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="GsbBundle/Form/Compta/FicheFraisType.html">FicheFraisType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:GsbBundle_Form_ConsulterFicheType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Form/ConsulterFicheType.html">ConsulterFicheType</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Form_FraisForfait2Type" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Form/FraisForfait2Type.html">FraisForfait2Type</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Form_LigneFraisForfaitType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Form/LigneFraisForfaitType.html">LigneFraisForfaitType</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Form_LigneFraisHorsForfaitType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Form/LigneFraisHorsForfaitType.html">LigneFraisHorsForfaitType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:GsbBundle_Repository" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Repository.html">Repository</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_Repository_EtatRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Repository/EtatRepository.html">EtatRepository</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Repository_FicheFraisRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Repository/FicheFraisRepository.html">FicheFraisRepository</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Repository_FraisForfait2Repository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Repository/FraisForfait2Repository.html">FraisForfait2Repository</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Repository_LigneFraisForfaitRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Repository/LigneFraisForfaitRepository.html">LigneFraisForfaitRepository</a>                    </div>                </li>                            <li data-name="class:GsbBundle_Repository_LigneFraisHorsForfaitRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Repository/LigneFraisHorsForfaitRepository.html">LigneFraisHorsForfaitRepository</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:GsbBundle_Services" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="GsbBundle/Services.html">Services</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:GsbBundle_Services_FicheFraisManager" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="GsbBundle/Services/FicheFraisManager.html">FicheFraisManager</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:GsbBundle_GsbBundle" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="GsbBundle/GsbBundle.html">GsbBundle</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "GsbBundle.html", "name": "GsbBundle", "doc": "Namespace GsbBundle"},{"type": "Namespace", "link": "GsbBundle/Controller.html", "name": "GsbBundle\\Controller", "doc": "Namespace GsbBundle\\Controller"},{"type": "Namespace", "link": "GsbBundle/DependencyInjection.html", "name": "GsbBundle\\DependencyInjection", "doc": "Namespace GsbBundle\\DependencyInjection"},{"type": "Namespace", "link": "GsbBundle/Entity.html", "name": "GsbBundle\\Entity", "doc": "Namespace GsbBundle\\Entity"},{"type": "Namespace", "link": "GsbBundle/Form.html", "name": "GsbBundle\\Form", "doc": "Namespace GsbBundle\\Form"},{"type": "Namespace", "link": "GsbBundle/Form/Compta.html", "name": "GsbBundle\\Form\\Compta", "doc": "Namespace GsbBundle\\Form\\Compta"},{"type": "Namespace", "link": "GsbBundle/Repository.html", "name": "GsbBundle\\Repository", "doc": "Namespace GsbBundle\\Repository"},{"type": "Namespace", "link": "GsbBundle/Services.html", "name": "GsbBundle\\Services", "doc": "Namespace GsbBundle\\Services"},
            
            {"type": "Class", "fromName": "GsbBundle\\Controller", "fromLink": "GsbBundle/Controller.html", "link": "GsbBundle/Controller/ComptaController.html", "name": "GsbBundle\\Controller\\ComptaController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_accueilAction", "name": "GsbBundle\\Controller\\ComptaController::accueilAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_administrerAction", "name": "GsbBundle\\Controller\\ComptaController::administrerAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_afficherAction", "name": "GsbBundle\\Controller\\ComptaController::afficherAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_ficheTraiterAction", "name": "GsbBundle\\Controller\\ComptaController::ficheTraiterAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_fichePayerAction", "name": "GsbBundle\\Controller\\ComptaController::fichePayerAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\ComptaController", "fromLink": "GsbBundle/Controller/ComptaController.html", "link": "GsbBundle/Controller/ComptaController.html#method_ajouterffAction", "name": "GsbBundle\\Controller\\ComptaController::ajouterffAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Controller", "fromLink": "GsbBundle/Controller.html", "link": "GsbBundle/Controller/GsbController.html", "name": "GsbBundle\\Controller\\GsbController", "doc": "&quot;Class GsbController&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Controller\\GsbController", "fromLink": "GsbBundle/Controller/GsbController.html", "link": "GsbBundle/Controller/GsbController.html#method_indexAction", "name": "GsbBundle\\Controller\\GsbController::indexAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\GsbController", "fromLink": "GsbBundle/Controller/GsbController.html", "link": "GsbBundle/Controller/GsbController.html#method_consulterFicheAction", "name": "GsbBundle\\Controller\\GsbController::consulterFicheAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\GsbController", "fromLink": "GsbBundle/Controller/GsbController.html", "link": "GsbBundle/Controller/GsbController.html#method_saisieFicheAction", "name": "GsbBundle\\Controller\\GsbController::saisieFicheAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Controller", "fromLink": "GsbBundle/Controller.html", "link": "GsbBundle/Controller/SecurityController.html", "name": "GsbBundle\\Controller\\SecurityController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Controller\\SecurityController", "fromLink": "GsbBundle/Controller/SecurityController.html", "link": "GsbBundle/Controller/SecurityController.html#method_LoginAction", "name": "GsbBundle\\Controller\\SecurityController::LoginAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\SecurityController", "fromLink": "GsbBundle/Controller/SecurityController.html", "link": "GsbBundle/Controller/SecurityController.html#method_checkAction", "name": "GsbBundle\\Controller\\SecurityController::checkAction", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Controller\\SecurityController", "fromLink": "GsbBundle/Controller/SecurityController.html", "link": "GsbBundle/Controller/SecurityController.html#method_logoutAction", "name": "GsbBundle\\Controller\\SecurityController::logoutAction", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\DependencyInjection", "fromLink": "GsbBundle/DependencyInjection.html", "link": "GsbBundle/DependencyInjection/GsbBundleExtension.html", "name": "GsbBundle\\DependencyInjection\\GsbBundleExtension", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\DependencyInjection\\GsbBundleExtension", "fromLink": "GsbBundle/DependencyInjection/GsbBundleExtension.html", "link": "GsbBundle/DependencyInjection/GsbBundleExtension.html#method_load", "name": "GsbBundle\\DependencyInjection\\GsbBundleExtension::load", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/Etat.html", "name": "GsbBundle\\Entity\\Etat", "doc": "&quot;Etat&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_getId", "name": "GsbBundle\\Entity\\Etat::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_setSigle", "name": "GsbBundle\\Entity\\Etat::setSigle", "doc": "&quot;Set sigle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_getSigle", "name": "GsbBundle\\Entity\\Etat::getSigle", "doc": "&quot;Get sigle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_setLibelle", "name": "GsbBundle\\Entity\\Etat::setLibelle", "doc": "&quot;Set libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_getLibelle", "name": "GsbBundle\\Entity\\Etat::getLibelle", "doc": "&quot;Get libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method___construct", "name": "GsbBundle\\Entity\\Etat::__construct", "doc": "&quot;Constructor&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_addEtatFicheFrai", "name": "GsbBundle\\Entity\\Etat::addEtatFicheFrai", "doc": "&quot;Add etatFicheFrai&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_removeEtatFicheFrai", "name": "GsbBundle\\Entity\\Etat::removeEtatFicheFrai", "doc": "&quot;Remove etatFicheFrai&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\Etat", "fromLink": "GsbBundle/Entity/Etat.html", "link": "GsbBundle/Entity/Etat.html#method_getEtatFicheFrais", "name": "GsbBundle\\Entity\\Etat::getEtatFicheFrais", "doc": "&quot;Get etatFicheFrais&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/FicheFrais.html", "name": "GsbBundle\\Entity\\FicheFrais", "doc": "&quot;FicheFrais&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method___construct", "name": "GsbBundle\\Entity\\FicheFrais::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getId", "name": "GsbBundle\\Entity\\FicheFrais::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setMois", "name": "GsbBundle\\Entity\\FicheFrais::setMois", "doc": "&quot;Set mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getMois", "name": "GsbBundle\\Entity\\FicheFrais::getMois", "doc": "&quot;Get mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setNbJustificatifs", "name": "GsbBundle\\Entity\\FicheFrais::setNbJustificatifs", "doc": "&quot;Set nbJustificatifs&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getNbJustificatifs", "name": "GsbBundle\\Entity\\FicheFrais::getNbJustificatifs", "doc": "&quot;Get nbJustificatifs&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setMontantValide", "name": "GsbBundle\\Entity\\FicheFrais::setMontantValide", "doc": "&quot;Set montantValide&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getMontantValide", "name": "GsbBundle\\Entity\\FicheFrais::getMontantValide", "doc": "&quot;Get montantValide&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setDateModif", "name": "GsbBundle\\Entity\\FicheFrais::setDateModif", "doc": "&quot;Set dateModif&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getDateModif", "name": "GsbBundle\\Entity\\FicheFrais::getDateModif", "doc": "&quot;Get dateModif&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setIdVisiteur", "name": "GsbBundle\\Entity\\FicheFrais::setIdVisiteur", "doc": "&quot;Set idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getIdVisiteur", "name": "GsbBundle\\Entity\\FicheFrais::getIdVisiteur", "doc": "&quot;Get idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_setIdEtat", "name": "GsbBundle\\Entity\\FicheFrais::setIdEtat", "doc": "&quot;Set idEtat&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FicheFrais", "fromLink": "GsbBundle/Entity/FicheFrais.html", "link": "GsbBundle/Entity/FicheFrais.html#method_getIdEtat", "name": "GsbBundle\\Entity\\FicheFrais::getIdEtat", "doc": "&quot;Get idEtat&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/FraisForfait2.html", "name": "GsbBundle\\Entity\\FraisForfait2", "doc": "&quot;FraisForfait2&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method___construct", "name": "GsbBundle\\Entity\\FraisForfait2::__construct", "doc": "&quot;Constructor&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_getId", "name": "GsbBundle\\Entity\\FraisForfait2::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_setSigle", "name": "GsbBundle\\Entity\\FraisForfait2::setSigle", "doc": "&quot;Set sigle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_getSigle", "name": "GsbBundle\\Entity\\FraisForfait2::getSigle", "doc": "&quot;Get sigle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_setLibelle", "name": "GsbBundle\\Entity\\FraisForfait2::setLibelle", "doc": "&quot;Set libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_getLibelle", "name": "GsbBundle\\Entity\\FraisForfait2::getLibelle", "doc": "&quot;Get libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_setMontant", "name": "GsbBundle\\Entity\\FraisForfait2::setMontant", "doc": "&quot;Set montant&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_getMontant", "name": "GsbBundle\\Entity\\FraisForfait2::getMontant", "doc": "&quot;Get montant&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_addCollectionFF", "name": "GsbBundle\\Entity\\FraisForfait2::addCollectionFF", "doc": "&quot;Add collectionFF&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_removeCollectionFF", "name": "GsbBundle\\Entity\\FraisForfait2::removeCollectionFF", "doc": "&quot;Remove collectionFF&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\FraisForfait2", "fromLink": "GsbBundle/Entity/FraisForfait2.html", "link": "GsbBundle/Entity/FraisForfait2.html#method_getCollectionFF", "name": "GsbBundle\\Entity\\FraisForfait2::getCollectionFF", "doc": "&quot;Get collectionFF&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/LigneFraisForfait.html", "name": "GsbBundle\\Entity\\LigneFraisForfait", "doc": "&quot;ligneFraisForfait&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_getId", "name": "GsbBundle\\Entity\\LigneFraisForfait::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_setMois", "name": "GsbBundle\\Entity\\LigneFraisForfait::setMois", "doc": "&quot;Set mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_getMois", "name": "GsbBundle\\Entity\\LigneFraisForfait::getMois", "doc": "&quot;Get mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_setQuantite", "name": "GsbBundle\\Entity\\LigneFraisForfait::setQuantite", "doc": "&quot;Set quantite&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_getQuantite", "name": "GsbBundle\\Entity\\LigneFraisForfait::getQuantite", "doc": "&quot;Get quantite&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_setIdVisiteur", "name": "GsbBundle\\Entity\\LigneFraisForfait::setIdVisiteur", "doc": "&quot;Set idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_getIdVisiteur", "name": "GsbBundle\\Entity\\LigneFraisForfait::getIdVisiteur", "doc": "&quot;Get idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_setIdFraisForfait", "name": "GsbBundle\\Entity\\LigneFraisForfait::setIdFraisForfait", "doc": "&quot;Set idFraisForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisForfait", "fromLink": "GsbBundle/Entity/LigneFraisForfait.html", "link": "GsbBundle/Entity/LigneFraisForfait.html#method_getIdFraisForfait", "name": "GsbBundle\\Entity\\LigneFraisForfait::getIdFraisForfait", "doc": "&quot;Get idFraisForfait&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait", "doc": "&quot;LigneFraisHorsForfait&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method___construct", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getId", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getId", "doc": "&quot;Get id&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setMois", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setMois", "doc": "&quot;Set mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getMois", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getMois", "doc": "&quot;Get mois&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setLibelle", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setLibelle", "doc": "&quot;Set libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getLibelle", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getLibelle", "doc": "&quot;Get libelle&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setDate", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setDate", "doc": "&quot;Set date&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getDate", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getDate", "doc": "&quot;Get date&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setMontant", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setMontant", "doc": "&quot;Set montant&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getMontant", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getMontant", "doc": "&quot;Get montant&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setIdVisiteur", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setIdVisiteur", "doc": "&quot;Set idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getIdVisiteur", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getIdVisiteur", "doc": "&quot;Get idVisiteur&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_getRefuser", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::getRefuser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\LigneFraisHorsForfait", "fromLink": "GsbBundle/Entity/LigneFraisHorsForfait.html", "link": "GsbBundle/Entity/LigneFraisHorsForfait.html#method_setRefuser", "name": "GsbBundle\\Entity\\LigneFraisHorsForfait::setRefuser", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Entity", "fromLink": "GsbBundle/Entity.html", "link": "GsbBundle/Entity/User.html", "name": "GsbBundle\\Entity\\User", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method___construct", "name": "GsbBundle\\Entity\\User::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_addUserLigneFraisForfait", "name": "GsbBundle\\Entity\\User::addUserLigneFraisForfait", "doc": "&quot;Add userLigneFraisForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_removeUserLigneFraisForfait", "name": "GsbBundle\\Entity\\User::removeUserLigneFraisForfait", "doc": "&quot;Remove userLigneFraisForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_getUserLigneFraisForfait", "name": "GsbBundle\\Entity\\User::getUserLigneFraisForfait", "doc": "&quot;Get userLigneFraisForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_addUserLigneFraisHorsForfait", "name": "GsbBundle\\Entity\\User::addUserLigneFraisHorsForfait", "doc": "&quot;Add userLigneFraisHorsForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_removeUserLigneFraisHorsForfait", "name": "GsbBundle\\Entity\\User::removeUserLigneFraisHorsForfait", "doc": "&quot;Remove userLigneFraisHorsForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_getUserLigneFraisHorsForfait", "name": "GsbBundle\\Entity\\User::getUserLigneFraisHorsForfait", "doc": "&quot;Get userLigneFraisHorsForfait&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_addUserFicheFrai", "name": "GsbBundle\\Entity\\User::addUserFicheFrai", "doc": "&quot;Add userFicheFrai&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_removeUserFicheFrai", "name": "GsbBundle\\Entity\\User::removeUserFicheFrai", "doc": "&quot;Remove userFicheFrai&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Entity\\User", "fromLink": "GsbBundle/Entity/User.html", "link": "GsbBundle/Entity/User.html#method_getUserFicheFrais", "name": "GsbBundle\\Entity\\User::getUserFicheFrais", "doc": "&quot;Get userFicheFrais&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form\\Compta", "fromLink": "GsbBundle/Form/Compta.html", "link": "GsbBundle/Form/Compta/AdministrerHorsForfaitType.html", "name": "GsbBundle\\Form\\Compta\\AdministrerHorsForfaitType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\AdministrerHorsForfaitType", "fromLink": "GsbBundle/Form/Compta/AdministrerHorsForfaitType.html", "link": "GsbBundle/Form/Compta/AdministrerHorsForfaitType.html#method_buildForm", "name": "GsbBundle\\Form\\Compta\\AdministrerHorsForfaitType::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form\\Compta", "fromLink": "GsbBundle/Form/Compta.html", "link": "GsbBundle/Form/Compta/ComptaConsulterType.html", "name": "GsbBundle\\Form\\Compta\\ComptaConsulterType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\ComptaConsulterType", "fromLink": "GsbBundle/Form/Compta/ComptaConsulterType.html", "link": "GsbBundle/Form/Compta/ComptaConsulterType.html#method_buildForm", "name": "GsbBundle\\Form\\Compta\\ComptaConsulterType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\ComptaConsulterType", "fromLink": "GsbBundle/Form/Compta/ComptaConsulterType.html", "link": "GsbBundle/Form/Compta/ComptaConsulterType.html#method_configureOptions", "name": "GsbBundle\\Form\\Compta\\ComptaConsulterType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form\\Compta", "fromLink": "GsbBundle/Form/Compta.html", "link": "GsbBundle/Form/Compta/FicheFraisType.html", "name": "GsbBundle\\Form\\Compta\\FicheFraisType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\FicheFraisType", "fromLink": "GsbBundle/Form/Compta/FicheFraisType.html", "link": "GsbBundle/Form/Compta/FicheFraisType.html#method_buildForm", "name": "GsbBundle\\Form\\Compta\\FicheFraisType::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\FicheFraisType", "fromLink": "GsbBundle/Form/Compta/FicheFraisType.html", "link": "GsbBundle/Form/Compta/FicheFraisType.html#method_configureOptions", "name": "GsbBundle\\Form\\Compta\\FicheFraisType::configureOptions", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\Compta\\FicheFraisType", "fromLink": "GsbBundle/Form/Compta/FicheFraisType.html", "link": "GsbBundle/Form/Compta/FicheFraisType.html#method_getBlockPrefix", "name": "GsbBundle\\Form\\Compta\\FicheFraisType::getBlockPrefix", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form", "fromLink": "GsbBundle/Form.html", "link": "GsbBundle/Form/ConsulterFicheType.html", "name": "GsbBundle\\Form\\ConsulterFicheType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\ConsulterFicheType", "fromLink": "GsbBundle/Form/ConsulterFicheType.html", "link": "GsbBundle/Form/ConsulterFicheType.html#method___construct", "name": "GsbBundle\\Form\\ConsulterFicheType::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\ConsulterFicheType", "fromLink": "GsbBundle/Form/ConsulterFicheType.html", "link": "GsbBundle/Form/ConsulterFicheType.html#method_buildForm", "name": "GsbBundle\\Form\\ConsulterFicheType::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form", "fromLink": "GsbBundle/Form.html", "link": "GsbBundle/Form/FraisForfait2Type.html", "name": "GsbBundle\\Form\\FraisForfait2Type", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\FraisForfait2Type", "fromLink": "GsbBundle/Form/FraisForfait2Type.html", "link": "GsbBundle/Form/FraisForfait2Type.html#method_buildForm", "name": "GsbBundle\\Form\\FraisForfait2Type::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\FraisForfait2Type", "fromLink": "GsbBundle/Form/FraisForfait2Type.html", "link": "GsbBundle/Form/FraisForfait2Type.html#method_configureOptions", "name": "GsbBundle\\Form\\FraisForfait2Type::configureOptions", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\FraisForfait2Type", "fromLink": "GsbBundle/Form/FraisForfait2Type.html", "link": "GsbBundle/Form/FraisForfait2Type.html#method_getBlockPrefix", "name": "GsbBundle\\Form\\FraisForfait2Type::getBlockPrefix", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form", "fromLink": "GsbBundle/Form.html", "link": "GsbBundle/Form/LigneFraisForfaitType.html", "name": "GsbBundle\\Form\\LigneFraisForfaitType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\LigneFraisForfaitType", "fromLink": "GsbBundle/Form/LigneFraisForfaitType.html", "link": "GsbBundle/Form/LigneFraisForfaitType.html#method_buildForm", "name": "GsbBundle\\Form\\LigneFraisForfaitType::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Form", "fromLink": "GsbBundle/Form.html", "link": "GsbBundle/Form/LigneFraisHorsForfaitType.html", "name": "GsbBundle\\Form\\LigneFraisHorsForfaitType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Form\\LigneFraisHorsForfaitType", "fromLink": "GsbBundle/Form/LigneFraisHorsForfaitType.html", "link": "GsbBundle/Form/LigneFraisHorsForfaitType.html#method_buildForm", "name": "GsbBundle\\Form\\LigneFraisHorsForfaitType::buildForm", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\LigneFraisHorsForfaitType", "fromLink": "GsbBundle/Form/LigneFraisHorsForfaitType.html", "link": "GsbBundle/Form/LigneFraisHorsForfaitType.html#method_configureOptions", "name": "GsbBundle\\Form\\LigneFraisHorsForfaitType::configureOptions", "doc": "&quot;{@inheritdoc}&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Form\\LigneFraisHorsForfaitType", "fromLink": "GsbBundle/Form/LigneFraisHorsForfaitType.html", "link": "GsbBundle/Form/LigneFraisHorsForfaitType.html#method_getBlockPrefix", "name": "GsbBundle\\Form\\LigneFraisHorsForfaitType::getBlockPrefix", "doc": "&quot;{@inheritdoc}&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle", "fromLink": "GsbBundle.html", "link": "GsbBundle/GsbBundle.html", "name": "GsbBundle\\GsbBundle", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\GsbBundle", "fromLink": "GsbBundle/GsbBundle.html", "link": "GsbBundle/GsbBundle.html#method_getParent", "name": "GsbBundle\\GsbBundle::getParent", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Repository", "fromLink": "GsbBundle/Repository.html", "link": "GsbBundle/Repository/EtatRepository.html", "name": "GsbBundle\\Repository\\EtatRepository", "doc": "&quot;EtatRepository&quot;"},
                    
            {"type": "Class", "fromName": "GsbBundle\\Repository", "fromLink": "GsbBundle/Repository.html", "link": "GsbBundle/Repository/FicheFraisRepository.html", "name": "GsbBundle\\Repository\\FicheFraisRepository", "doc": "&quot;FicheFraisRepository&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Repository\\FicheFraisRepository", "fromLink": "GsbBundle/Repository/FicheFraisRepository.html", "link": "GsbBundle/Repository/FicheFraisRepository.html#method_getFicheUserForm", "name": "GsbBundle\\Repository\\FicheFraisRepository::getFicheUserForm", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "GsbBundle\\Repository", "fromLink": "GsbBundle/Repository.html", "link": "GsbBundle/Repository/FraisForfait2Repository.html", "name": "GsbBundle\\Repository\\FraisForfait2Repository", "doc": "&quot;FraisForfait2Repository&quot;"},
                    
            {"type": "Class", "fromName": "GsbBundle\\Repository", "fromLink": "GsbBundle/Repository.html", "link": "GsbBundle/Repository/LigneFraisForfaitRepository.html", "name": "GsbBundle\\Repository\\LigneFraisForfaitRepository", "doc": "&quot;LigneFraisForfaitRepository&quot;"},
                    
            {"type": "Class", "fromName": "GsbBundle\\Repository", "fromLink": "GsbBundle/Repository.html", "link": "GsbBundle/Repository/LigneFraisHorsForfaitRepository.html", "name": "GsbBundle\\Repository\\LigneFraisHorsForfaitRepository", "doc": "&quot;LigneFraisHorsForfaitRepository&quot;"},
                    
            {"type": "Class", "fromName": "GsbBundle\\Services", "fromLink": "GsbBundle/Services.html", "link": "GsbBundle/Services/FicheFraisManager.html", "name": "GsbBundle\\Services\\FicheFraisManager", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "GsbBundle\\Services\\FicheFraisManager", "fromLink": "GsbBundle/Services/FicheFraisManager.html", "link": "GsbBundle/Services/FicheFraisManager.html#method___construct", "name": "GsbBundle\\Services\\FicheFraisManager::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Services\\FicheFraisManager", "fromLink": "GsbBundle/Services/FicheFraisManager.html", "link": "GsbBundle/Services/FicheFraisManager.html#method_ficheVisiteur", "name": "GsbBundle\\Services\\FicheFraisManager::ficheVisiteur", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "GsbBundle\\Services\\FicheFraisManager", "fromLink": "GsbBundle/Services/FicheFraisManager.html", "link": "GsbBundle/Services/FicheFraisManager.html#method_creationFicheFrais", "name": "GsbBundle\\Services\\FicheFraisManager::creationFicheFrais", "doc": "&quot;&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


