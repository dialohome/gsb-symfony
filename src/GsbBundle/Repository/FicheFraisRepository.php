<?php

namespace GsbBundle\Repository;

/**
 * FicheFraisRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FicheFraisRepository extends \Doctrine\ORM\EntityRepository
{

    public function getFicheCloturation($etat1, $etat2, $mois)
    {
        $gb = $this->createQueryBuilder('e')
            ->where("e.idEtat = :etat1 OR e.idEtat = :etat2")
                ->setParameter('etat1', $etat1)
                ->setParameter('etat2', $etat2)
            ->andWhere("e.mois = :mois")
                ->setParameter('mois', $mois);
        return $gb
            ->getQuery()
            ->getResult()
            ;
    }
}
