<?php
/**
 * Created by PhpStorm.
 * User: Guillaume
 * Date: 27/10/2016
 * Time: 16:26
 */

namespace GsbBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="membres")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="FicheFrais", mappedBy="idVisiteur", cascade={"remove", "persist"})
     */
    private $userFicheFrais;

    /**
     * @ORM\OneToMany(targetEntity="LigneFraisForfait", mappedBy="idVisiteur", cascade={"remove", "persist"})
     */
    private $userLigneFraisForfait;

    /**
     * @ORM\OneToMany(targetEntity="LigneFraisHorsForfait", mappedBy="idVisiteur", cascade={"remove", "persist"})
     */
    private $userLigneFraisHorsForfait;
    
    public function __construct()
    {
        parent:: __construct();
    }

    /**
     * Add userLigneFraisForfait
     *
     * @param \GsbBundle\Entity\LigneFraisForfait $userLigneFraisForfait
     *
     * @return User
     */
    public function addUserLigneFraisForfait(\GsbBundle\Entity\LigneFraisForfait $userLigneFraisForfait)
    {
        $this->userLigneFraisForfait[] = $userLigneFraisForfait;

        return $this;
    }

    /**
     * Remove userLigneFraisForfait
     *
     * @param \GsbBundle\Entity\LigneFraisForfait $userLigneFraisForfait
     */
    public function removeUserLigneFraisForfait(\GsbBundle\Entity\LigneFraisForfait $userLigneFraisForfait)
    {
        $this->userLigneFraisForfait->removeElement($userLigneFraisForfait);
    }

    /**
     * Get userLigneFraisForfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserLigneFraisForfait()
    {
        return $this->userLigneFraisForfait;
    }

    /**
     * Add userLigneFraisHorsForfait
     *
     * @param \GsbBundle\Entity\LigneFraisHorsForfait $userLigneFraisHorsForfait
     *
     * @return User
     */
    public function addUserLigneFraisHorsForfait(\GsbBundle\Entity\LigneFraisHorsForfait $userLigneFraisHorsForfait)
    {
        $this->userLigneFraisHorsForfait[] = $userLigneFraisHorsForfait;

        return $this;
    }

    /**
     * Remove userLigneFraisHorsForfait
     *
     * @param \GsbBundle\Entity\LigneFraisHorsForfait $userLigneFraisHorsForfait
     */
    public function removeUserLigneFraisHorsForfait(\GsbBundle\Entity\LigneFraisHorsForfait $userLigneFraisHorsForfait)
    {
        $this->userLigneFraisHorsForfait->removeElement($userLigneFraisHorsForfait);
    }

    /**
     * Get userLigneFraisHorsForfait
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserLigneFraisHorsForfait()
    {
        return $this->userLigneFraisHorsForfait;
    }

    /**
     * Add userFicheFrai
     *
     * @param \GsbBundle\Entity\FicheFrais $userFicheFrai
     *
     * @return User
     */
    public function addUserFicheFrai(\GsbBundle\Entity\FicheFrais $userFicheFrai)
    {
        $this->userFicheFrais[] = $userFicheFrai;

        return $this;
    }

    /**
     * Remove userFicheFrai
     *
     * @param \GsbBundle\Entity\FicheFrais $userFicheFrai
     */
    public function removeUserFicheFrai(\GsbBundle\Entity\FicheFrais $userFicheFrai)
    {
        $this->userFicheFrais->removeElement($userFicheFrai);
    }

    /**
     * Get userFicheFrais
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserFicheFrais()
    {
        return $this->userFicheFrais;
    }
}
