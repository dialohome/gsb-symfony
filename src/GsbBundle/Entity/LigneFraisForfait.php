<?php

namespace GsbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ligneFraisForfait
 *
 * @ORM\Table(name="ligne_frais_forfait")
 * @ORM\Entity(repositoryClass="GsbBundle\Repository\LigneFraisForfaitRepository")
 */
class LigneFraisForfait
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userLigneFraisForfait", cascade={"persist"})
     * @ORM\JoinColumn(name="idVisiteur", referencedColumnName="id")
     */
    private $idVisiteur;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=255)
     */
    private $mois;

    /**
     * @ORM\ManyToOne(targetEntity="FraisForfait2", inversedBy="collectionFF", cascade={"persist"})
     * @ORM\JoinColumn(name="idFraisForfait", referencedColumnName="id")
     */
    private $idFraisForfait;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mois
     *
     * @param string $mois
     *
     * @return LigneFraisForfait
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Get mois
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return LigneFraisForfait
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set idVisiteur
     *
     * @param \GsbBundle\Entity\User $idVisiteur
     *
     * @return LigneFraisForfait
     */
    public function setIdVisiteur(\GsbBundle\Entity\User $idVisiteur = null)
    {
        $this->idVisiteur = $idVisiteur;

        return $this;
    }

    /**
     * Get idVisiteur
     *
     * @return \GsbBundle\Entity\User
     */
    public function getIdVisiteur()
    {
        return $this->idVisiteur;
    }

    /**
     * Set idFraisForfait
     *
     * @param \GsbBundle\Entity\FraisForfait2 $idFraisForfait
     *
     * @return LigneFraisForfait
     */
    public function setIdFraisForfait(\GsbBundle\Entity\FraisForfait2 $idFraisForfait = null)
    {
        $this->idFraisForfait = $idFraisForfait;

        return $this;
    }

    /**
     * Get idFraisForfait
     *
     * @return \GsbBundle\Entity\FraisForfait2
     */
    public function getIdFraisForfait()
    {
        return $this->idFraisForfait;
    }
}
