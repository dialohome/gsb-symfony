<?php

namespace GsbBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
* FraisForfait2
*
* @ORM\Table(name="frais_forfait2")
* @ORM\Entity(repositoryClass="GsbBundle\Repository\FraisForfait2Repository")
*/
class FraisForfait2
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="LigneFraisForfait", mappedBy="idFraisForfait", fetch="LAZY")
     */
    private $collectionFF;

    /**
     * @var string
     *
     * @ORM\Column(name="sigle", type="string", length=255)
     */
    private $sigle;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="decimal", precision=8, scale=2)
     */
    private $montant;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->collectionFF = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sigle
     *
     * @param string $sigle
     *
     * @return FraisForfait2
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * Get sigle
     *
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return FraisForfait2
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return FraisForfait2
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Add collectionFF
     *
     * @param \GsbBundle\Entity\LigneFraisForfait $collectionFF
     *
     * @return FraisForfait2
     */
    public function addCollectionFF(\GsbBundle\Entity\LigneFraisForfait $collectionFF)
    {
        $this->collectionFF[] = $collectionFF;

        return $this;
    }

    /**
     * Remove collectionFF
     *
     * @param \GsbBundle\Entity\LigneFraisForfait $collectionFF
     */
    public function removeCollectionFF(\GsbBundle\Entity\LigneFraisForfait $collectionFF)
    {
        $this->collectionFF->removeElement($collectionFF);
    }

    /**
     * Get collectionFF
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollectionFF()
    {
        return $this->collectionFF;
    }
}
