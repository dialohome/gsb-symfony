<?php

namespace GsbBundle\Services;

use Doctrine\ORM\EntityManager;
use GsbBundle\Entity\FicheFrais;
use Symfony\Component\HttpFoundation\Session\Session;

class FicheFraisManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function ficheVisiteur($idVisiteur, $mois)
    {
        $em = $this->em;
        $repo = $em->getRepository('GsbBundle:FicheFrais');
        $fiche = $repo->findOneBy(array('idVisiteur' => $idVisiteur, 'mois' => $mois));
        return $fiche;
    }

    public function creationFicheFrais($idVisiteur, $mois, $nbJustificatifs = null)
    {
        $session = new Session();
        $em = $this->em;
        $repoEtat = $em->getRepository('GsbBundle:Etat');
        if ($nbJustificatifs == null) {
            $repoLfhf = $em->getRepository('GsbBundle:LigneFraisHorsForfait');
            $nbJustificatifs = count($repoLfhf->findBy(array('idVisiteur' => $idVisiteur, 'mois' => $mois)));
        }
        $fiche = new FicheFrais();
        $fiche->setIdVisiteur($idVisiteur);
        $fiche->setNbJustificatifs($nbJustificatifs);
        $fiche->setMois($mois);
        $fiche->setIdEtat($repoEtat->find(2));
        $em->persist($fiche);
        $em->flush();
        $session->getFlashBag()->add('success', 'La fiche de frais à été créée');
    }
}