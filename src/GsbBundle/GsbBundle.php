<?php

namespace GsbBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class GsbBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
