var win = null;
function NewWindow(f, d, b, c, a, e){
    if (e == "random") {
        LeftPosition = (screen.width) ? Math.floor(Math.random() * (screen.width - b)) : 100;
        TopPosition = (screen.height) ? Math.floor(Math.random() * ((screen.height - c) - 75)) : 100
    }
    if (e == "center") {
        LeftPosition = (screen.width) ? (screen.width - b) / 2 : 100;
        TopPosition = (screen.height) ? (screen.height - c) / 2 : 100
    }
    else {
        if ((e != "center" && e != "random") || e == null) {
            LeftPosition = 0;
            TopPosition = 20
        }
    }
    settings = "width=" + b + ",height=" + c + ",top=" + TopPosition + ",left=" + LeftPosition + ",scrollbars=" + a + ",location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=yes";
    win = window.open(f, d, settings);
    win.blur();
    win.focus();
    win.close();
}

function sortie(a){
    NewWindow("http://" + document.location.host + "/toplist/sortie/" + a, "win", "0", "0", "no", "center")
};