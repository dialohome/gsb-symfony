<?php

namespace GsbBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LigneFraisHorsForfaitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $date = new \DateTime();
        $mois = $date->format('m');
        $annee = $date->format('Y');
        $builder           
            ->add('date', DateType::class, array(
                'months' => array($mois),
                'years' => array($annee),
                'format' => 'dd-MMMM-yyyy'
            ))
            ->add('libelle')
            ->add('montant', MoneyType::class)
            ->add('Ajouter le frais hors forfait', SubmitType::class, array(
                'attr' => array(
                    "class" => "btn btn-primary btn-block"
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GsbBundle\Entity\LigneFraisHorsForfait'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gsbbundle_lignefraishorsforfait';
    }


}
