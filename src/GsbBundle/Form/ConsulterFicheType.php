<?php

namespace GsbBundle\Form;

use GsbBundle\Repository\FicheFraisRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ConsulterFicheType extends AbstractType
{

    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
     $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Fiche', EntityType::class, array(
                'class' => 'GsbBundle:FicheFrais',
                'query_builder' => function (FicheFraisRepository $er) {
                  return $er->createQueryBuilder('u')
                      ->where('u.idVisiteur = '.$this->tokenStorage->getToken()->getUser()->getId())
                      ->orderBy('u.mois', 'DESC');
                },
                'choice_label' => 'mois'
            ))
            ->add('Afficher', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
            ;
    }
}
