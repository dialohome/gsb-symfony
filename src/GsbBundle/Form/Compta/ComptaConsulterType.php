<?php

namespace GsbBundle\Form\Compta;

use GsbBundle\Repository\FicheFraisRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComptaConsulterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('User', EntityType::class, array(
                'class' => 'GsbBundle:User',
                'choice_label' => 'username',
                'label' => 'Choisissez l\'utilisateur à administrer',
            ));
        if($options['user']) {
            $user = $options['user'];
            $builder
                ->add('Fiche', EntityType::class, array(
                    'class' => 'GsbBundle:FicheFrais',
                    'query_builder' => function (FicheFraisRepository $er) use ($user) {
                        return $er->createQueryBuilder('u')
                            ->where('u.idVisiteur = '.$user)
                            ->orderBy('u.mois', 'DESC');
                },
                    'choice_label' => 'mois',
                    'choice_value' => 'mois',
                    'label' => 'Sélectionnez la date de la fiche à administrer',
                    'label_attr' => array(
                        'id' => 'label_mois'
                    ),

            ));
        } else {
            $builder
                ->add('Fiche', ChoiceType::class, array(
                        'choices' => array(
                            'Sélectionnez avant tout un utilisateur' => null
                        ),
                        'label' => 'Sélectionnez la date de la fiche à administrer',
                        'label_attr' => array(
                            'id' => 'label_mois'
                        )
                    )
                );
        }
        $builder
            ->add('Afficher', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-success'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
        ));
    }

}
