<?php

namespace GsbBundle\Form\Compta;

use GsbBundle\Repository\EtatRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FicheFraisType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montantValide')
            ->add('idEtat', EntityType::class, array(
                'class' => 'GsbBundle:Etat',
                'choice_label' => 'libelle',
                'choice_value' => 'libelle',
            ))
            ->add('Valider', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-success pull-right'
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GsbBundle\Entity\FicheFrais'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gsbbundle_fichefrais';
    }


}
