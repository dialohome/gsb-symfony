<?php

namespace GsbBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;


class LigneFraisForfaitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("quantiteFE", IntegerType::class, array(
                "label" => "Forfait Etape",
                "attr" => array(
                    'placeholder' => "Quantité de Forfait Etape"
                )
            ))
            ->add("quantiteFK", IntegerType::class, array(
                "label" => "Frais Kilométrique",
                "attr" => array(
                    'placeholder' => "Quantité de kilometres"
                )
            ))
            ->add("quantiteNH", IntegerType::class, array(
                "label" => "Nuitée Hôtel",
                "attr" => array(
                    'placeholder' => "Quantité nuité hotel"
                )
            ))
            ->add("quantiteRR", IntegerType::class, array(
                "label" => "Repas restaurant",
                "attr" => array(
                    'placeholder' => "Quantité de repas restaurant"
                )
            ))
            ->add("Valider ou modifier mes quantites en forfait", SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary btn-block'
                )
            ))
        ;
    }
    
}
