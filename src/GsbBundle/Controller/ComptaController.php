<?php

namespace GsbBundle\Controller;

use GsbBundle\Entity\FicheFrais;
use GsbBundle\Form\Compta\ComptaConsulterType;
use GsbBundle\Form\Compta\FicheFraisType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ComptaController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function accueilAction()
    {
        return $this->render('GsbBundle:comptabilite:accueil.html.twig');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clotureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GsbBundle:FicheFrais');
        $repoEtat = $em->getRepository('GsbBundle:Etat');
        $get = $request->request;
        $cloture = $get->get('cloture');
        // RECUPERATION DE LA DATE ET DU MOIS PRECEDENT
        $date = new \DateTime();
        $mois = $date->modify('-1 month');
        $mois = $mois->format('m-Y');
        // RECUPERATION DES OBJETS ETAT POUR LA REQUETE DQL
        $etatCloture = $repoEtat->find(1);
        $etatEnCours = $repoEtat->find(2);
        $list = $repo->getFicheCloturation($etatCloture, $etatEnCours, $mois);
        // VERIFICATION DU BOUTON S'IL EST VALIDE OU ANNULER PUIS LANCEMENT DU TRAITEMENT DES FICHES
        if ($request->getMethod() == 'POST') {
            if ($cloture == "valider") {
                foreach ($list as $fiche) {
                    if ($fiche->getIdEtat() == $etatEnCours) {
                        $fiche->setIdEtat($etatCloture);
                        $fiche->setDateModif($date);
                    }
                }
                $this->addFlash('success', 'Les fiches portant le libellé "Fiche créée et saisie en cours" du mois précédent ont été cloturées');
            } else if ($cloture == "annuler") {
                foreach ($list as $fiche) {
                    if ($fiche->getIdEtat() == $etatCloture) {
                        $fiche->setIdEtat($etatEnCours);
                        $fiche->setDateModif($date);
                    }
                }
                $this->addFlash('success', 'Les fiches portant le libellé "Saisie clôturée" du mois précédent ont été réouverte');
            }
            $em->flush();
        }
        // VERIFICATION DES FICHE SI ELLES SONT BIEN CLOTUREES POUR EFFACER LE BOUTON VALIDER ET LE REMPLACER PAR ANNULER DANS LE VUE
        $verif = false;
        foreach ($list as $fiche) {
            if ($fiche->getIdEtat()->getId() == 1) $verif = true;
        }
        return $this->render('GsbBundle:comptabilite:cloture.html.twig', array(
            'date' => $mois,
            'cloture' => $verif,
            'list' => $list
        ));
    }

    /**
     * @param Request $request
     * @param FicheFrais $fiche
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function administrerAction(Request $request, FicheFrais $fiche)
    {
        if (!isset($fiche)) return $this->redirectToRoute('gsb_compta_afficher');
        $em = $this->getDoctrine()->getManager();

        $repoLff = $em->getRepository('GsbBundle:LigneFraisForfait');
        $repoLfhf = $em->getRepository('GsbBundle:LigneFraisHorsForfait');
        $repoFraisF = $em->getRepository('GsbBundle:FraisForfait2');

        // ADMINISTRATION DES LIGNES HORS FORFAIT
        $get = $request->request;
        // ACCEPTER DES FRAIS HORS FORFAIT
        if ($request->getMethod() == "POST" && $get->get('accepter_hors_forfait') > 0) {
            $id = $get->get('accepter_hors_forfait');
            $ligne = $repoLfhf->find($id);
            if ($ligne->getRefuser() == true) {
                $ligne->setRefuser(false);
                $em->persist($ligne);
                $em->flush();
                $this->addFlash('success', 'Le frais hors forfait a été à nouveau accepté dans la comptabilité');
            } else $this->addFlash('danger', 'Le frais hors forfait a déjà le statut validé');

        } //
        // REFUSER DES FRAIS HORS FORFAIT
        if ($request->getMethod() == 'POST' && $get->get('refuser_hors_forfait') > 0) {
            $id = $get->get('refuser_hors_forfait');
            $ligne = $repoLfhf->find($id);
            if ($ligne->getRefuser() == false) {
                $ligne->setRefuser(true);
                $em->persist($ligne);
                $em->flush();
                $this->addFlash('success', 'Le frais hors forfait a été refusé dans la comptabilité, le salaré en sera informé');
            } else $this->addFlash('danger', 'Le frais hors forfait a déjà le statut refusé');

        } //
        // REPORTER LE TRAITEMENT D'UNE LIGNE HORS FORFAIT
        if ($request->getMethod() == 'POST' && $get->get('reporter_hors_forfait')) {
            $id = $get->get('reporter_hors_forfait');
            $id = $repoLfhf->find($id);
            $mois = $id->getMois();
            $moisDecoupe = explode("-", $mois);
            $moisSuivant = $moisDecoupe[0] + 1;
            $dateReport = $moisSuivant . '-' . $moisDecoupe[1];
            $id->setMois($dateReport);
            $fiche->setNbJustificatifs($fiche->getNbJustificatifs() - 1); // ON ABAISSE DE 1 LE NOMBRE DE JUSTIFICATIF DU MOIS
            $em->flush();
            $this->addFlash('success', 'Le frais hors forfait a été reporté au mois suivant');
            // CREATION DE LA FICHE DE FRAIS DU MOIS SUIVANT SI ELLE N'EXISTE PAS ENCORE ET AJOUT D'UN JUSTIFICATIF POUR CELLE CI (manip effectuée dans le manager)
            $ficheFraisManager = $this->get('gsb.fichefrais');
            $verifFicheFrais = $ficheFraisManager->ficheVisiteur($fiche->getIdVisiteur(), $dateReport);
            if (!isset($verifFicheFrais)) {
                $ficheFraisManager->creationFicheFrais($fiche->getIdVisiteur(), $dateReport);
            }
        } //
        // FIN ADMINISTRATION HORS FORFAIT //

        // Creation du formulaire et administrer la fiche de frais
        $formFiche = $this->createForm(FicheFraisType::class, $fiche);
        $formFiche->handleRequest($request);
        if ($formFiche->isSubmitted() and $formFiche->isValid()) {
            /* if ($fiche->getIdEtat()->getId() == 4) {
                $this->get('knp_snappy.pdf')->generateFromHtml(
                    $this->renderView('GsbBundle:comptabilite:fichepdf.html.twig'),
                    __DIR__."/../Resources/public/pdf/test.pdf"
                );
            } */
            $em->persist($fiche);
            $em->flush();
            $this->addFlash('success', 'La fiche a bien été modifiée');
        }

        $listLff = $repoLff->findBy(array('idVisiteur' => $fiche->getIdVisiteur(), 'mois' => $fiche->getMois()));
        $listLfhf = $repoLfhf->findBy(array('idVisiteur' => $fiche->getIdVisiteur(), 'mois' => $fiche->getMois()));
        $listFraisF = $repoFraisF->findAll();

        // Calcul des frais forfait renseignés
        $totalForfait = 0;
        $tabMontantForfait = array();
        foreach ($listFraisF as $frais) {
            $montant = 0;
            foreach ($listLff as $ligne) {
                $forfait = $frais->getMontant();
                $quantite = $ligne->getQuantite();
                $id1 = $ligne->getIdFraisForfait()->getId();
                $id2 = $frais->getId();
                if ($id1 == $id2) {
                    $montant = $forfait * $quantite;
                    $totalForfait += $montant;
                    $tabMontantForfait[$id2] = $montant;
                }
            }
        }

        // Calcul des frais hors forfait
        $totalHorsForfait = 0;
        foreach ($listLfhf as $horsForfait) {
            if ($horsForfait->getRefuser() == false) $totalHorsForfait += $horsForfait->getMontant();
        }

        // Calcul des frais totaux
        $totalFrais = $totalHorsForfait + $totalForfait;

        return $this->render('GsbBundle:comptabilite:administrer.html.twig', array(
            'formFiche' => $formFiche->createView(),
            'ficheF' => $fiche,
            'ligneFf' => $listLff,
            'ligneFhf' => $listLfhf,
            'tabMontantForfait' => $tabMontantForfait,
            'totalForfait' => $totalForfait,
            'totalHorsForfait' => $totalHorsForfait,
            'totalFrais' => $totalFrais
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function afficherAction(Request $request)
    {
        $form = $this->createForm(ComptaConsulterType::class);
        $form->handleRequest($request);
        $get = $request->get('compta_consulter');
        if (isset($get['User'])) $user = $get['User'];
        if (isset($get['Fiche'])) $fiche = $get['Fiche'];

        if (isset($user)) {
            $form = $this->createForm(ComptaConsulterType::class, null, array(
                'user' => $user
            ));
        }
        if (isset($user) and isset($fiche)) {
            $em = $this->getDoctrine()->getManager();

            $repoFiche = $em->getRepository('GsbBundle:FicheFrais');
            $repoLff = $em->getRepository('GsbBundle:LigneFraisForfait');
            $repoLfhf = $em->getRepository('GsbBundle:LigneFraisHorsForfait');

            $ficheF = $repoFiche->findOneBy(array('idVisiteur' => $user, 'mois' => $fiche));
            $listLff = $repoLff->findBy(array('idVisiteur' => $user, 'mois' => $fiche));
            $listLfhf = $repoLfhf->findBy(array('idVisiteur' => $user, 'mois' => $fiche));

            if (!isset($ficheF)) {
                $this->addFlash('danger', 'Cette fiche ne semble pas exister');
            }

            return $this->render('GsbBundle:comptabilite:afficher.html.twig', array(
                'form' => $form->createView(),
                'ficheF' => $ficheF,
                'ligneFf' => $listLff,
                'ligneFhf' => $listLfhf,
            ));
        }

        if ($request->getMethod() == "POST" && $request->get('createPdf')) {
            if ($request->get('idFiche')) {
                $nombreAleatoire = rand(00001, 99999);
                $html = $this->renderView("GsbBundle:comptabilite:fichepdf.html.twig", array('fiche' => array("test" => "test")));
                $this->get('knp_snappy.pdf')->generateFromHtml($html , dirname(__DIR__) . '/Resources/public/pdf/' . $nombreAleatoire . '.pdf'
                );
                return new JsonResponse($nombreAleatoire.'.pdf');
            }
            else return new JsonResponse(false);
        }


        return $this->render('GsbBundle:comptabilite:afficher.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ficheTraiterAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GsbBundle:FicheFrais');
        $listFicheFrais = $repo->findBy(array('idEtat' => 1));
        return $this->render('GsbBundle:comptabilite:ficheTraiter.html.twig', array(
            'list' => $listFicheFrais
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fichePayerAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GsbBundle:FicheFrais');
        $repoEtat = $em->getRepository('GsbBundle:Etat');
        $get = $request->request;
        $payer = $get->get('payer');
        if ($request->getMethod() == "POST" && isset($payer)) {
            $fiche = $repo->find($payer);
            $fiche->setIdEtat($repoEtat->find(3));
            $em->flush();
            $this->addFlash('success', '(----PAIEMENT FICTIF----) La fiche de frais est passé au statut Remboursé');
        }
        return $this->render('GsbBundle:comptabilite:fichePayer.html.twig', array(
            'list' => $repo->findBy(array('idEtat' => 4))
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ficheSuspendueAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GsbBundle:FicheFrais');
        $repoEtat = $em->getRepository('GsbBundle:Etat');
        return $this->render('GsbBundle:comptabilite:ficheSuspendu.html.twig', array(
            'list' => $repo->findByIdEtat($repoEtat->find(5))
        ));
    }

}