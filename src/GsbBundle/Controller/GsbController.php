<?php

namespace GsbBundle\Controller;

use GsbBundle\Entity\FraisForfait2;
use GsbBundle\Entity\LigneFraisForfait;
use GsbBundle\Entity\LigneFraisHorsForfait;
use GsbBundle\Form\ConsulterFicheType;
use GsbBundle\Form\LigneFraisForfaitType;
use GsbBundle\Form\LigneFraisHorsForfaitType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GsbController
 * @package GsbBundle\Controller
 */
class GsbController extends Controller
{
    /**
     * @Route("/", name="accueil")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $repoFf = $em->getRepository('GsbBundle:FicheFrais');
        $repoFhf = $em->getRepository('GsbBundle:LigneFraisHorsForfait');
        $repoFraisForfait = $em->getRepository('GsbBundle:FraisForfait2');
        
        $date = new \DateTime();
        $date = $date->modify('-1month');
        $date = $date->format('m-Y');
        $ficheFrais = $repoFf->findOneBy(array('idVisiteur' => $this->getUser(), 'mois' => $date));
        $listFhf = $repoFhf->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $date));
        $listFraisForfait = $repoFraisForfait->findAll();
        
        return $this->render('GsbBundle:gsb:index.html.twig', array(
            'ficheF' => $ficheFrais,
            'listFhf' => $listFhf,
            'listFraisForfait' => $listFraisForfait
        ));
    }

    /**
     * @Route("/user/consulter", name="consulter_fiche")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function consulterFicheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repoLigne = $em->getRepository('GsbBundle:FicheFrais');

        if(count($repoLigne->findBy(array('idVisiteur' => $this->getUser()))) > 0 ) {
            $form = $this->createForm(ConsulterFicheType::class);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $get = $request->request;
                $idFiche = $get->get('consulter_fiche');

                $repoF = $em->getRepository('GsbBundle:LigneFraisForfait');
                $repoHF = $em->getRepository('GsbBundle:LigneFraisHorsForfait');
                $fiche = $repoLigne->findOneById($idFiche['Fiche']);
                $ligneF = $repoF->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $fiche->getMois()));
                $ligneHF = $repoHF->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $fiche->getMois()));

                return $this->render('GsbBundle:gsb:consulter.html.twig', array(
                    'form' => $form->createView(),
                    'fiche' => $fiche,
                    'ligneF' => $ligneF,
                    'ligneHF' => $ligneHF
                ));
            }
            return $this->render('GsbBundle:gsb:consulter.html.twig', array(
                'form' => $form->createView()
            ));
        } else {
            return $this->render('GsbBundle:gsb:consulter.html.twig');
        }
    }

    /**
     * @Route("/user/saisir", name="saisie_fiche")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saisieFicheAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime();
        $mois = $date->format("m-Y");

        // REPOSITORY UTILES
        $repoLff = $em->getRepository('GsbBundle:LigneFraisForfait');
        $repoFf = $em->getRepository('GsbBundle:FraisForfait2');
        $repoLfhf = $em->getRepository('GsbBundle:LigneFraisHorsForfait');
        $repoFicheFrais = $em->getRepository('GsbBundle:FicheFrais');
        $repoEtat = $em->getRepository('GsbBundle:Etat');
        
        // SERIVCE FICHE FRAIS
        $ficheFraisManager = $this->get('gsb.fichefrais');

        // CREATION DU FORM POUR LES 4 LIGNES DE FRAIS EN FORFAIT
        $form = $this->createForm(LigneFraisForfaitType::class);
        $form->handleRequest($request);

        // VERIFICATION SI LES LIGNES ONT DEJA ETE AJOUTEES AFIN DE LES EDITER ET NON LES CREER
        $verifLff = $repoLff->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $mois));


        // DEBUT DE LA SOUMISSION DU FORMULAIRE
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $get = $form->getData(); // RECUPERATION DES DONNEES RENSEIGNEES DANS LE FORM
                $i = 1; // INDICE POUR LES BOUCLES
                // SI LES LIGNES EXISTENT ON LES EDIT
                if (count($verifLff) > 0) {
                    foreach ($get as $quantite) {
                       foreach ($verifLff as $ligneFF) {
                           $idFraisForfait = $ligneFF->getIdFraisForfait()->getId();
                           if($idFraisForfait == $i) {
                               $ligneFF->setQuantite($quantite);
                           }
                       }
                        $i+=1;
                    }
                    $this->addFlash('success', 'Les eléments forfaitisés ont été modifiés');

                    // ET JE MODIFIE LA FICHE DE FRAIS
                    $ficheFrais = $repoFicheFrais->findOneBy(array('mois' => $mois, 'idVisiteur' => $this->getUser())); // FICHES DE FRAIS EXISTANTE
                    $ficheFrais->setDateModif(new \DateTime());
                    $em->persist($ficheFrais);
                    $this->addFlash('success', 'La fiche de frais a été modifiée');

                } else { // SINON ON CREES LES LIGNES
                    foreach ($get as $quantite) {
                        $objetFraisForfait = $repoFf->findOneById($i);
                        $ligne = new LigneFraisForfait();
                        $ligne->setIdFraisForfait($objetFraisForfait);
                        $ligne->setMois($mois);
                        $ligne->setIdVisiteur($this->getUser());
                        $ligne->setQuantite($quantite);
                        $em->persist($ligne);
                        $i+=1;
                    }
                    $this->addFlash('success', 'L\'ajout a été effectué');
                    
                    // ON VERIFIE SI IL Y A DEJA UNE FICHE DE FRAIS
                    $ficheFrais = $ficheFraisManager->ficheVisiteur($this->getUser(), $mois);
                    // SI IL Y EN A PAS ON CREE ALORS LA FICHE DE FRAIS
                    if (!$ficheFrais) {
                        $ficheFraisManager->creationFicheFrais($this->getUser(), $mois, 0);
                    } else {
                        // SINON JE METS A JOUR LA FICHE FRAIS
                        $ficheFrais->setDateModif(new \DateTime());
                        $em->persist($ficheFrais);
                        $this->addFlash('success', 'La fiche de frais a été modifiée');
                    }
                }
            }
        }

        // CREATION D'UN NOUVEL OBJET LIGNE HORS FORFAIT ASSEMBLE AU FORMULAIRE
        $ligneHF = new LigneFraisHorsForfait();
        $formHF = $this->createForm(LigneFraisHorsForfaitType::class, $ligneHF);
        $formHF->handleRequest($request);
        // DEBUT DE SOUMISSION DU FORMLULAIRE
        if ($formHF->isSubmitted()) {
            if ($formHF->isValid()) {
                // SI TOUT EST BON ON AJOUTE LA LIGNE DEMANDEES
                $ligneHF->setMois($mois);
                $ligneHF->setIdVisiteur($this->getUser());
                $em->persist($ligneHF);
                $this->addFlash('success', 'L\'élément hors fofait a été ajouté');

                // ON VERIFIE SI IL Y A DEJA UNE FICHE DE FRAIS
                $ficheFrais = $ficheFraisManager->ficheVisiteur($this->getUser(), $mois);
                // SI IL Y EN A PAS ON CREE ALORS LA FICHE DE FRAIS
                if (!$ficheFrais) {
                    $ficheFraisManager->creationFicheFrais($this->getUser(), $mois, 1);
                } else {
                    // SINON JE METS A JOUR LA FICHE FRAIS
                    $ficheFrais = $ficheFraisManager->ficheVisiteur($this->getUser(), $mois);
                    $nombreJustificatifs = $ficheFrais->getNbJustificatifs();
                    $ficheFrais->setDateModif(new \DateTime());
                    $ficheFrais->setNbJustificatifs($nombreJustificatifs+1);
                    $em->persist($ficheFrais);
                    $this->addFlash('success', 'La fiche de frais a été modifiée');
                }
            }
        }

        // SUPRESSION D'UNE LIGNE FRAIS HORS FORFAIT PAR LE VISITEUR
        $get = $request->request;
        $idDelete = $get->get('supprimer_hors_forfait');
        if ($request->getMethod() == 'POST' && isset($idDelete)) {
            $ligneDelete = $repoLfhf->find($idDelete);
            $em->remove($ligneDelete);
            $this->addFlash('success', 'La ligne hors forfait a été supprimée');
        }

        // ON FLUSH TTES LES MANIPS
        $em->flush();

        // ON CHARGE LES DONNEES POUR LES AFFICHER
        $lff = $repoLff->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $mois));
        $lhf = $repoLfhf->findBy(array('idVisiteur' => $this->getUser(), 'mois' => $mois), array('date' => 'DESC'));

        return $this->render('GsbBundle:gsb:saisieFiche.html.twig', array(
            'form' => $form->createView(),
            'formHF' => $formHF->createView(),
            'ligneForfait' => $lff,
            'ligneHorsForfait' => $lhf
        ));
    }
}