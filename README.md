# GSB PROJET SOUS SYMFONY

Développé par Guillaume LANGOUET étudiant en BAC +2 informatique

## Description

Le projet propose aux salariés de l'entreprise GSB de déclarer leurs frais de déplacement durant leur travail.

- Frais Kilométriques
- Repas
- Hotels

L'application permet aussi aux comptables de la société d'effectuer la comptabilité, le controle et le paiement des frais déclarés par les salariés et de les informer en cas d'anomalie.